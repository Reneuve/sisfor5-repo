/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tugas2;

import javax.swing.JOptionPane;
import java.lang.Math;

/**
 *
 * @author Rafael
 */
public class Tugas2 {

    /**
     * @param args the command line arguments
     */
    static boolean repeat = true;
    static final int convert = 100;

    public static class UserData {

        double tinggi_user;
        double berat_user;
        double result;
        double berat_rec;
        int need_day;

        UserData(double tinggi, double berat) {
            this.tinggi_user = Math.pow((tinggi / convert), 2);
            this.berat_user = berat;
        }

        public String get_results() {
            result = berat_user / tinggi_user;
            if (result <= 18.5) {
                berat_rec = Math.round((18.5 * tinggi_user) - berat_user);
                need_day = (int) (berat_rec) * 3;
//                berat_rec = Math.round(18.5 - result);
            } else if (result >= 23) {
                berat_rec = Math.abs((Math.round(((25 * tinggi_user) - berat_user))));
                need_day = (int) (berat_rec) * 7;
//                berat_rec = Math.round(result - 22.9);
            } else {
                berat_rec = 0;
            }
            System.out.println(result);
            System.out.println((int) (berat_rec));
            if (result < 18.5) {
                return "Berat badan kurang \n"
                        + "- Lakukan penambahan berat sebanyak " + berat_rec + " Kg \n"
                        + "- Waktu untuk mencapai berat badan ideal anda adalah " + need_day + " hari";
            } else if (result >= 18.5 && result <= 22.9) {
                return "Berat badan normal";
            } else if (result >= 23 && result <= 29.9) {
                return "Berat badan berlebih (kecenderungan obesitas) \n"
                        + "- Lakukan pengurangan berat sebanyak " + berat_rec + " Kg \n"
                        + "- Waktu untuk mencapai berat badan ideal anda adalah " + need_day + " hari";
            } else if (result >= 30) {
                return "Obesitas \n"
                        + "- Lakukan pengurangan berat sebanyak " + berat_rec + " Kg \n"
                        + "- Waktu untuk mencapai berat badan ideal anda adalah " + need_day + " hari";
            } else {
                return "Terjadi kesalahan";
            }
        }
    }

    static double func_input(String params) {
        String choice = JOptionPane.showInputDialog(params);
        if (choice == null) {
            System.exit(0);
            return 0;
        } else {
            try {
                return Double.parseDouble(
                        choice
                );
            } catch (NumberFormatException e) {
                double new_val = func_input(params);
                return new_val;
            }
//            return Double.parseDouble(
//                choice
//            );
        }
    }

    public static void main(String[] args) {

        // TODO code application logic here
        while (repeat) {
            System.out.println("Start");
            double tinggi = func_input("Masukan Tinggi Badan:   (cm)");
            double berat = func_input("Masukan Berat Badan:   (kg)");
            UserData user = new UserData(tinggi, berat);
            int answer = JOptionPane.showConfirmDialog(null, "Anda Yakin?");
            switch (answer) {
                /* answer range
                 0 :yes
                 1 : no
                 2: cancel
                 */
                case JOptionPane.YES_OPTION:
                    JOptionPane.showMessageDialog(null, user.get_results());
                    func_again();
                    break;
                case JOptionPane.NO_OPTION:
                    break;
                case JOptionPane.CANCEL_OPTION:
                    System.exit(0);
            }
        }
    }

    static void func_again() {
        try {
            String again = JOptionPane.showInputDialog("Ingin mencoba lagi? (y/n)");
            if (again.equalsIgnoreCase("y") || again.equalsIgnoreCase("n")) {
                if (again.equalsIgnoreCase("n")) {
                    repeat = false;
                }
            } else {
                func_again();
            }
        } catch (Exception e) {
            System.exit(0);
        }
    }

}
